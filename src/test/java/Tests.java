import me.ghui.designpattern.builder.NutritionFacts;
import me.ghui.designpattern.singleton.*;
import org.junit.Test;

/**
 * Created by vann on 3/8/15.
 */
public class Tests {

    @Test
    public void testSingleton1() {
        Singleton1.getInstance();
    }

    @Test
    public void testSingleton2() {
        Singleton2.getInstance();
    }

    @Test
    public void testSingleton3() {
        Singleton3.getInstance();
    }
    @Test
    public void testSingleton5() {
        //only init once
        Singleton5.getInstance();
        Singleton5.getInstance();
        Singleton5.getInstance();
        Singleton5.getInstance();
    }

    @Test
    public void testSingleton6() {
        Singleton6.INSTANCE.print();
    }

    @Test
    public void testBuilder() {
        NutritionFacts nutritionFacts = new NutritionFacts.Builder(1, 2).c(3).d(4).build();
        System.err.println(nutritionFacts.toString());
    }
}
