package me.ghui.designpattern.strategy;

/**
 * Created by vann on 6/22/14.
 * ????????????????????????????????????????????????????????????????
 */
public class StrategyPattern {
    public static void main(String[] args) {
        Client client = new Client(new GunBehavior());
        client.showBehavior();
        client.setWeapon(new SwordBehavior()); //???????
        client.showBehavior();
    }
}

/*
Context
 */
class Client{
    private WeaponBehavior weapon;

    public Client(WeaponBehavior weapon) {
        this.weapon = weapon;
    }

    public void setWeapon(WeaponBehavior weapon) {
        this.weapon = weapon;
    }

    public void showBehavior() {
        weapon.play();
    }
}
/*
abstract strategy interface
 */
interface WeaponBehavior{
    void play();
}

/*
concrete strategy class
 */
class GunBehavior implements WeaponBehavior {

    @Override
    public void play() {
        System.out.println("GunBehavior...");
    }
}

class SwordBehavior implements WeaponBehavior{

    @Override
    public void play() {
        System.out.println("SwordBehavior...");
    }
}