package me.ghui.designpattern.builder;

/**
 * Created by vann on 3/8/15.
 */
public class NutritionFacts {

    //required parameters
    private int a;
    private int b;
    //optional parameters
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;

    @Override public String toString() {
        return "NutritionFacts{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", d=" + d +
                ", e=" + e +
                ", f=" + f +
                ", g=" + g +
                '}';
    }

    private NutritionFacts(Builder builder) {
        this.a = builder.a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
        this.e = builder.e;
    }

    public static class Builder {
        private int a;
        private int b;
        private int c = 0;
        private int d = 0;
        private int e = 0;

        public Builder(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public Builder c(int c) {
            this.c = c;
            return this;
        }

        public Builder d(int d) {
            this.d = d;
            return this;
        }

        public Builder e(int e) {
            this.e = e;
            return this;
        }

        public NutritionFacts build() {
            return new NutritionFacts(this);
        }
    }
}
