package me.ghui.designpattern.singleton;

/**3: 双重检查锁  在jdk1.5之后失效 与新的内存模型相关 (加上volatile ok)
 * instance = new Singleton3();
 * a.给 instance 分配内存
   b.调用 Singleton 的构造函数来初始化成员变量
   c.将instance对象指向分配的内存空间（执行完这步 instance 就为非 null 了）

 abc,acb .加上volatile后能保证 （volatile 变量的写操作都先行发生于后面对这个变量的读操作）
 */
public class Singleton3 {
    private volatile static Singleton3 instance;
    private Singleton3() {
        System.err.println("Singleton3 init ...");
    }
    public static Singleton3 getInstance() {
        if (instance == null) {
            synchronized (Singleton3.class) {
                if (instance == null) {
                    instance = new Singleton3(); //非原子操作
                }
            }
        }
        return instance;
    }
}
