package me.ghui.designpattern.singleton;

/**
 * Created by vann on 3/8/15.
 */ //2: 单重检查锁
public class Singleton2 {
    private static Singleton2 instance;
    private Singleton2() {
        System.err.println("Singleton2 init ... ");
    }
    public static synchronized Singleton2 getInstance() {
        if (instance == null) {
            instance = new Singleton2();
        }
        return instance;
    }
}
