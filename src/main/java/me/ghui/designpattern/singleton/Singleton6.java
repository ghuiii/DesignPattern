package me.ghui.designpattern.singleton;

/**
 * Created by vann on 3/8/15.
 */ //6: 枚举, 最佳实现 after jdk5
public enum Singleton6 {
    INSTANCE;
    public void print() {
        System.err.println("hello SingleTon6 !");
    }
}
