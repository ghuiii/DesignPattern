package me.ghui.designpattern.singleton;

/**
 * Created by vann on 3/8/15.
 */ //1: 普通
public class Singleton1 {
    private static Singleton1 instance;
    private Singleton1() {
        System.err.println("singleton1 init... ");
    }
    public static Singleton1 getInstance() {
        if (instance == null) {
            instance = new Singleton1();
        }
        return instance;
    }
}
