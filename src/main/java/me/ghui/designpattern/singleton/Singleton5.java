package me.ghui.designpattern.singleton;

/**
 * Created by vann on 3/8/15.
 */ //5: 静态内部类构造,推荐方法
public class Singleton5{
    private Singleton5() {
        System.err.println("Singleton5 init...");
    }
    private static class Nest{ //内部类直到他们被引用时才会加载
        private static final Singleton5 INSTANCE = new Singleton5();
    }
    public static Singleton5 getInstance() {
        return Nest.INSTANCE;
    }
}
