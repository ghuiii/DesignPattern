package me.ghui.designpattern.singleton;

/**
 * Created by vann on 3/8/15.
 */ //4: 提前加载
public class Singleton4{
    private Singleton4() {
        System.err.println("Singleton4 init ...");
    }
    private static final Singleton4 INSTANCE = new Singleton4();
    public static Singleton4 getInstance() {
        return INSTANCE;
    }
}
